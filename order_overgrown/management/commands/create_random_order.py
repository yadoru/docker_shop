from random import randrange
from django.core.management.base import BaseCommand
from order.models import Order, ProductOrder
from account.models import User
from delivery.models import Delivery
from product.models import Product


class Command(BaseCommand):
    #this command creates n number of users
    help = 'Takes one int type argument. Create n number of different products in order.' \
           ' If n is not specified the number of product is 5'

    def add_arguments(self, parser):
        parser.add_argument('number', type = int, nargs='?', default = 5)


    def handle(self, *args, **options):
        i =  options['number']
        k = 0
        user = User.objects.order_by('?').first()
        delivery = Delivery.objects.order_by('?').first()
        order = User.objects.filter(order__status=0).first()
        if order is not None:
            order = Order.objects.create(user=user)
        while k<i:
            product = Product.objects.order_by('?').first()
            amount = randrange(1, 10)
            prod_order = ProductOrder.objects.create(order=order, product = product, amount = amount)
            self.stdout.write(self.style.SUCCESS('Created product order for %s' % prod_order.product.title))
            k+=1
        self.stdout.write(self.style.SUCCESS('Successfully created order: %s' % order.id))
