from django.db import models
from django.utils.translation import gettext as _


class Order(models.Model):
    #Order takes user, delivery, status and is related in One to Many relation with ProductOrder.
    #There are 5 statuses: 0 - in creation, 1 - paid, 2 - in delivery, 3 - archived and 4 - complain.
    #Related name for ProductOrder is product_ordered
    #Default string is [name of user who ordered the goods | order_id]
    #delivery is nullable, but null only allowed for new order. It is not allowed to have null delivery for other status
    STATUS_CHOICES = [
        (0, _('in creation')),
        (1, _('paid')),
        (2, _('in delivery')),
        (3, _('delivered')),
        (4, _('complain')),
    ]
    user = models.ForeignKey('account.User', on_delete=models.CASCADE, related_name='order')
    delivery = models.ForeignKey('delivery.Delivery', on_delete=models.CASCADE, null=True, default=None)
    status = models.PositiveSmallIntegerField(verbose_name=_('status'), choices=STATUS_CHOICES, default=0)

    def __str__(self):
        str(self.user.username)+'|'+str(self.id)


class ProductOrder(models.Model):
    #assign product and amount of product ordered into single order. Default string is [title of product | order_id]
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(_('amount of product ordered'))
    order = models.ForeignKey('order.Order', on_delete=models.CASCADE, related_name='product_ordered')

    def __str__(self):
        return str(self.product.title) + " " + str(self.order.id)