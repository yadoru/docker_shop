from rest_framework import serializers
from .models import ProductOrder, Order

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductOrder
        fields = ('na', 'title', 'code', 'linenos', 'language', 'style')