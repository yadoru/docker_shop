# Rest for a simple shop app

Example of implementation of few rest endpoints in django project.
Project is automatically filled with random data.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine
 for development and testing purposes.
### Prerequisites

```
docker
docker-compose
```

### Installing

Download the package into desired location. Run command "docker-compose up" inside top most project folder.

Access docker container through following command
```
docker exec -it dockershop_web_1 /bin/bash
```


then, from inside of the docker container
```
python manage.py makemigrations
python manage.py migrate
```
###endpoints

Default endpoint is localhost:80. If other port is required the docker-compose.yml file have to be changed. In line 10

```
    command: python manage.py runserver 0.0.0.0:80

```

to

```
    command: python manage.py runserver 0.0.0.0:[desired port]

```

and line 15

```
    - "80:80"
```

to
```
    - "[desired port]:[desired port]"
```

## Populating database

There are a few commands to populate database. To run them, execute the following commands:
```
docker exec -it dockershop_web_1 /bin/bash
```

then, from inside of the docker container
```
python manage.py create_random_users
python manage.py create_random_categories
python manage.py create_random_products
python manage.py create_random_connections
python manage.py create_random_order
```


## Running the tests
Not implemented yet


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

