from datetime import timedelta

import oauth2_provider
from django.urls import reverse
from oauth2_provider.contrib.rest_framework import OAuth2Authentication
from rest_framework import status
from account import views
from account.models import User
from oauth2_provider.models import get_access_token_model, get_application_model
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.test import APITestCase

Application = get_application_model()
AccessToken = get_access_token_model()
UserModel = get_user_model()


def create_token(self, user):

    self.app = Application.objects.create(
        client_type=Application.CLIENT_CONFIDENTIAL,
        authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        redirect_uris='https://www.none.com/oauth2/callback',
        name='dummy',
        user=user
    )

    access_token = AccessToken.objects.create(
        user=user,
        scope='read write',
        expires=timezone.now() + timedelta(seconds=300),
        token='secret-access-token-key',
        application=self.app
    )
    return access_token


class AuthViewsTests(APITestCase):

    def setUp(self):
        self.username = 'usuario'
        self.password = 'contrasegna'
        self.email = 'usario@gmail.com'
        self.data = {
            'username': self.username,
            'password': self.password
        }
        self.user = User.objects.create_user(username=self.username, email=self.email,\
                                             password=self.password)


    def test_empty_wishlist(self):
        # URL using path name
        url = '/jwtauth/token/login'

        # Create a user is a workaround in order to authentication works
        self.assertEqual(self.user.is_active, 1, 'Active User')
        # First post to get token
        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        jwt_token = response.data['auth_token']
        self.assertContains(response, 'auth_token')
        oauth_token = create_token(self, user=self.user)
        # Next post/get's will require the token to connect
        self.client.credentials(HTTP_AUTHORIZATION='Token {0}'.format(jwt_token, oauth_token))
        response = self.client.get(reverse('wishlist'), data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)

