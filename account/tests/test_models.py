from django.test import TestCase
from ..models import User, Wishlist
from product.models import Category, Product
from delivery.models import Delivery
from order.models import Order

class UserTest(TestCase):
    """ Test module for User model """

    def setUp(self):
        User.objects.create(
            username = 'Marion', email="ingress@wp.pl", password='sasageyo', country='PL')
        User.objects.create(
            username = 'Miri', email="postgre@gmail.com", password='shinazawa', country='US')
        User.objects.create(
            username = 'Makaka', email="ikazuchi@o2.pl", password='shinzou', country='IT')
        Delivery.objects.create(
            delivery_type = 'UPS', delivery_price_USD=10.00, delivery_price_EUR=9.50, delivery_price_PLN=35.60
        )
        self.category = Category.objects.create(
            title = 'Songs'
        )
        Product.objects.create(
            title = 'Clearmind', category=self.category, image='monkey.jpg', price_USD=9.20, price_EUR=10, price_PLN=44.20,
        )

    def test_correct_country(self):
        user_miri = User.objects.get(username='Miri')
        user_marion = User.objects.get(username='Marion')
        user_makaka = User.objects.get(username='Makaka')
        self.assertEqual(
            user_miri.country, "US")
        self.assertEqual(
            user_marion.country, "PL")
        self.assertEqual(
            user_makaka.country, "IT")
