from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User
from order.models import Order


class OrderInline(admin.TabularInline):
    model = Order


class AccountAdmin(UserAdmin):
    inlines = [OrderInline]


admin.site.register(User, AccountAdmin)
