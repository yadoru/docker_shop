from django.db import models
from product.models import Product
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext as _

class User(AbstractUser):
    # allow user to have his liked products and set his country.
    COUNTRIES = [
        ('PL', _('Poland')),
        ('US', _('United States')),
        ('DE', _('Deutchland')),
        ('FR', _('France')),
        ('IT', _('Italy')),
    ]
    # could be implemented by django coutries plugin, here is a short example non plugin solution
    likes = models.ManyToManyField('product.Product', verbose_name=_('wish list'), related_name='likes',
                                   through='Wishlist')
    country = models.CharField(choices=COUNTRIES, verbose_name=_('Country'), max_length=2, default='US')

    def __str__(self):
        return self.username


class Wishlist(models.Model):
    user = models.ForeignKey('account.User', on_delete=models.CASCADE, related_name='user_to_product')
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE, related_name='product_to_user')
