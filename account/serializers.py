from rest_framework import serializers
from .models import User, Wishlist
from product.serializers import ProductSerializer


class CreateUserSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'password')
        extra_kwargs = {
            'password': {'write_only': True}
}


class UserSerializer(serializers.ModelSerializer):
    likes = ProductSerializer(many=True)

    class Meta:
        model = User
        fields = ('username', 'country', 'likes')
        read_only_fields = ('username', 'id')

class WishlistSerializer(serializers.ModelSerializer):
    product = ProductSerializer()
    user = UserSerializer()

    class Meta:
        model = Wishlist
        fields = ('user', 'product')