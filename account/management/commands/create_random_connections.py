from django.core.management.base import BaseCommand
from account.models import User, Wishlist
from product.models import Product


class Command(BaseCommand):
    # this command changes random number of connections between users and products.
    # Require existing users and products.
    help = 'Takes one int type argument. Change connections between users and products n.' \
           ' If n is not specified the quantity of product is 10. Require existing users and products'

    def add_arguments(self, parser):
        parser.add_argument('number', type = int, nargs='?', default = 10)


    def handle(self, *args, **options):
        n = options['number']
        i = 0
        while i < n:
            user = User.objects.order_by('?').first()
            product = Product.objects.order_by('?').first()
            try:
                Wishlist.objects.get(user=user, product=product).delete()
                self.stdout.write(self.style.SUCCESS('Deleted connection between %s and %s' %
                                                     (user, product)))
                i+=1
            except Wishlist.DoesNotExist:
                Wishlist.objects.create(user=user, product=product)
                self.stdout.write(self.style.SUCCESS('Created connection between %s and %s' %
                                                     (user, product)))
                i+=1
        self.stdout.write(self.style.SUCCESS('Successfully ended command'))
