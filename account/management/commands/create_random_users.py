from django.core.management.base import BaseCommand
from account.models import User
from account.helpers.generate_random_username import generate_random_username



class Command(BaseCommand):
    #this command creates n number of users
    help = 'Takes one int type argument. Create n number of random users.' \
           ' If n is not specified the number of random users created is 10'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int, nargs='?', default=10)

    def handle(self, *args, **options):
        i =  options['number']
        k = 0
        username = []
        while k<i:
            username.append(generate_random_username())
            user = User.objects.create_user(username[k], password='userpassword')
            user.is_superuser = False
            user.is_staff = False
            user.save()
            self.stdout.write(self.style.SUCCESS('Successfully created user "%s"' % username[k]))
            k+=1
        self.stdout.write(self.style.SUCCESS('Successfully executed the command'))
