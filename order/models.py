from django.db import models
from django.utils.translation import gettext as _
EUROPEAN_COUNTRIES = ['DE', 'FR', 'IT']


class Order(models.Model):
    # takes user from account app, delivery from delivery app, product from product app and quantity as positive
    # total price is allowed to be null, when null it means product or shipment is unavailable in country
    user = models.ForeignKey('account.User', on_delete=models.CASCADE, related_name='order')
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(_('quantity of product ordered'))
    delivery = models.ForeignKey('delivery.Delivery', on_delete=models.CASCADE)
    total_price = models.DecimalField(verbose_name=_('Total price'), decimal_places=2, max_digits=10, blank=True)

    # set total price depending on currency used in country we ship products to.
    def save(self, *args, **kwargs):
        if self.user.country == 'US':
            delivery_price = self.delivery.delivery_price_USD
            product_price = self.product.price_USD
        if self.user.country in EUROPEAN_COUNTRIES:
            delivery_price = self.delivery.delivery_price_EUR
            product_price = self.product.price_EUR
        if self.user.country == 'PL':
            delivery_price = self.delivery.delivery_price_PLN
            product_price = self.product.price_PLN
        self.total_price = delivery_price + product_price * self.quantity
        return super(Order, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id)