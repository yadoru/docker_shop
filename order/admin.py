from django.contrib import admin
from .models import Order


class OrderAdmin(admin.ModelAdmin):
    fields = ['user', 'product', 'delivery', 'quantity']
    list_display = ['id', 'product', 'category']
    search_fields = ['id', 'product__title']

    def category(self,obj):
        return obj.product.category.title

admin.site.register(Order, OrderAdmin)
