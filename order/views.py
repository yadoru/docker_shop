from order.serializers import OrderSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope


class OrderCreateView(generics.CreateAPIView):
    serializer_class = OrderSerializer
    permission_classes = (IsAuthenticated, TokenHasReadWriteScope)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
