from django.core.management.base import BaseCommand
from order.models import Order
from account.models import User
from delivery.models import Delivery
from product.models import Product


class Command(BaseCommand):
    #this command creates random order. Requires existing user, delivery, categories and products.
    help = 'Takes one int type argument. Create order for product with quantity n.' \
           ' If n is not specified the quantity of product is 1. Require existing products, users, categories and '\
           'delivery'

    def add_arguments(self, parser):
        parser.add_argument('number', type = int, nargs='?', default = 1)


    def handle(self, *args, **options):
        quantity =  options['number']
        user = User.objects.order_by('?').first()
        delivery = Delivery.objects.order_by('?').first()
        product = Product.objects.order_by('?').first()
        prod_order = Order.objects.create(product = product, quantity = quantity, delivery=delivery, user=user)
        self.stdout.write(self.style.SUCCESS('Created product order for %s' % prod_order.product.title))
        self.stdout.write(self.style.SUCCESS('Successfully created order: %s' % prod_order.id))
