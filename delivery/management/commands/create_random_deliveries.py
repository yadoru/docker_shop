from django.core.management.base import BaseCommand
from delivery.models import Delivery
from delivery.helpers.generate_random import generate_random_deliveryname, generate_random_price
class Command(BaseCommand):
    #this command creates n delivery types
    help = 'Takes one int type argument. Create n number of random delivery types.' \
           ' If n is not specified the number of random delivery types created is 3'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int, nargs='?', default=3)
    def handle(self, *args, **options):
        i =  options['number']
        k = 0
        type = []
        price_EUR = []
        price_PLN = []
        price_USD = []
        while k<i:
            type.append(generate_random_deliveryname())
            price_EUR.append(generate_random_price(100,2000))
            price_USD.append(generate_random_price(100,2500))
            price_PLN.append(generate_random_price(500,5000))
            Delivery.objects.create(delivery_type=type[k], delivery_price_EUR=price_EUR[k],\
                                    delivery_price_USD=price_USD[k], delivery_price_PLN=price_PLN[k])
            self.stdout.write(self.style.SUCCESS('Successfully created delivery "%s:%s€ %s$ %sPLN' % (type[k], \
                                                 price_EUR[k], price_USD[k], price_PLN[k])))
            k+=1
        self.stdout.write(self.style.SUCCESS('Successfully executed the command'))
