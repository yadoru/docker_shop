from _decimal import Decimal
from random import choice,randrange
from string import ascii_lowercase, digits
from delivery.models import Delivery

def generate_random_deliveryname(length=randrange(1,12), chars=ascii_lowercase + digits, split=4, delimiter='-'):
    name = ''.join([choice(chars) for i in range(length)])
    if split:
        name = delimiter.join([name[start:start + split] for start in range(0, len(name), split)])
    try:
        Delivery.objects.get(delivery_type=name)
        return generate_random_deliveryname(length=length, chars=chars, split=split, delimiter=delimiter)
    except Delivery.DoesNotExist:
        return name


def generate_random_price(a,b):
    price = randrange(a,b)/100
    return price
