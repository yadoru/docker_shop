from django.contrib import admin
from .models import Delivery

class DeliveryAdmin(admin.ModelAdmin):
    fields = ['delivery_type', 'delivery_price_USD', 'delivery_price_EUR', 'delivery_price_PLN']
    list_display = ['delivery_type', 'delivery_price_USD', 'delivery_price_EUR', 'delivery_price_PLN']
    search_fields = ['delivery_type']


admin.site.register(Delivery, DeliveryAdmin)

