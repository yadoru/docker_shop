from django.db import models
from django.utils.translation import gettext as _
#prices should be added for each new currency. Null means product is unavailable in currency
class Delivery(models.Model):
    delivery_type = models.CharField(_("Delivery type"), max_length=30)
    delivery_price_USD = models.DecimalField(_("price in USD"),  max_digits=4, decimal_places=2)
    delivery_price_EUR = models.DecimalField(_("price in EURO"), max_digits=4, decimal_places=2)
    delivery_price_PLN = models.DecimalField(_("price in PLN"),  max_digits=4, decimal_places=2)

    def __str__(self):
        return self.delivery_type+":"+str(self.delivery_price_USD)+"$ "+str(self.delivery_price_EUR)+"€ "+\
               str(self.delivery_price_PLN)+"PLN"

    class Meta:
        verbose_name_plural = "Deliveries"
