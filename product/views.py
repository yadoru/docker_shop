from .serializers import ProductSerializer
from .models import Product
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

class SmallSetPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 100

class ProductList(generics.ListAPIView):
    serializer_class = ProductSerializer
    pagination_class = SmallSetPagination
    queryset = Product.objects.all()
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,)
    filterset_fields = ('category', )
    search_fields = ('title', )
