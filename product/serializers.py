from rest_framework import serializers
from .models import Category, Product


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('name')
        read_only_fields = [f.name for f in Category._meta.get_fields()]


class ProductSerializer(serializers.ModelSerializer):
    category = serializers.StringRelatedField()

    class Meta:
        model = Product
        fields = ('title', 'image', 'description', 'price_USD', 'price_EUR', 'price_PLN', 'category')
        read_only_fields = [f.name for f in Product._meta.get_fields()]

