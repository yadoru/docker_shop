from _decimal import Decimal
from random import choice, randrange
from string import ascii_lowercase, digits
from product.models import Product

ninja = ['wa','ra','ya','ma','ha','na','ta','ka','a','ri','mi','hi','ni','ti','si','ki','i','wo','ru','yu','mu','hu',\
        'nu','tu','su','ku','u','re','me','he','ne','te','se','ke','e','n','ro','yo','mo','ho','to','so','ko','o']

def generate_random_word(length = randrange(2,7)):
    word = ''.join([choice(ninja) for i in range(length)])
    return word

def generate_random_name(length=randrange(1,12), chars=ascii_lowercase + digits, split=4, delimiter='-'):
    name = ''.join([choice(chars) for i in range(length)])
    if split:
        name = delimiter.join([name[start:start + split] for start in range(0, len(name), split)])
    try:
        Product.objects.get(title=name)
        return generate_random_name(length=length, chars=chars, split=split, delimiter=delimiter)
    except Product.DoesNotExist:
        return name

def generate_random_description(length=randrange(1,12)):
    a=''
    for i in range(length):
        for k in range(randrange(1,7)):
            a+=generate_random_word()+' '
        a=a[:-1]+'. '
    return a

def generate_random_price(a,b):
    price = randrange(a,b)/100
    return price

