from django.db import models
from django.utils.translation import gettext as _

def file_path(instance, filename):
    #simple function to make image go into category of product
    return '{0}/{1}'.format(instance.Category.title, filename)

class Category(models.Model):
    #category got only one field, title which have category name, default string for the category is title of category
    title = models.CharField(_('Category'), max_length=25)

    def __str__(self):
        return self.title


    class Meta:
        verbose_name_plural = 'Categories'

class Product(models.Model):
    #product have its own title, image which is uploaded into [category/filename], description, price in different
    #currencies(currently only $, € and PLN), and category, default string rep. is [category/title]
    title = models.CharField(_('Product Name'), max_length=100,unique= True)
    image = models.FileField(_('Image'), upload_to=file_path)
    description = models.TextField(_('Description'))
    price_USD = models.DecimalField(_('price in $'),max_digits=8, decimal_places=2, null=True, blank=True)
    price_EUR = models.DecimalField(_('price in €'),max_digits=8, decimal_places=2, null=True, blank=True)
    price_PLN = models.DecimalField(_('price in PLN'),max_digits=8, decimal_places=2, null=True, blank=True)
    category = models.ForeignKey(Category, verbose_name=_('category'), on_delete=models.CASCADE)


    def __str__(self):
        return self.category.title+" | "+self.title

