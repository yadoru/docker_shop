from django.contrib import admin
from .models import Product, Category


class ProductAdmin(admin.ModelAdmin):
    fields = ['title', 'image', 'description', 'price_USD', 'price_EUR', 'price_PLN', 'category']
    list_display = ['title', 'category']
    search_fields = ['title']


class CategoryAdmin(admin.ModelAdmin):
    fields = ['title']
    search_fields = ['title']



admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
