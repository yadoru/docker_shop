from django.core.management.base import BaseCommand
from product.models import Product, Category
from product.helpers.generate_random import generate_random_description, generate_random_name,\
                                            generate_random_price
class Command(BaseCommand):
    # this command creates n number of random products with default image.
    # requires existing categories to work
    help = 'Takes one int type argument. Create n number of random products.' \
           ' If n is not specified the number of random products created is 20'\
           'Require existing categories.'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int, nargs='?', default=20)
    def handle(self, *args, **options):
        i =  options['number']
        k = 0
        titles = []
        price_EUR = []
        price_PLN = []
        price_USD = []
        category  = []
        description = []
        while k<i:
            titles.append(generate_random_name())
            price_EUR.append(generate_random_price(100,2000))
            price_USD.append(generate_random_price(100,2500))
            price_PLN.append(generate_random_price(500,5000))
            category.append(Category.objects.order_by("?").first())
            description.append(generate_random_description())
            Product.objects.create(title=titles[k], price_EUR=price_EUR[k], category=category[k],\
                                   price_USD=price_USD[k], price_PLN=price_PLN[k], description=description[k],\
                                   image='monkey.jpg')
            self.stdout.write(self.style.SUCCESS('Successfully created product "%s %s:%s€ %s$ %sPLN' % (category[k],\
                                                                titles[k], price_EUR[k], price_USD[k], price_PLN[k])))
            k+=1
        self.stdout.write(self.style.SUCCESS('Successfully executed the command'))
