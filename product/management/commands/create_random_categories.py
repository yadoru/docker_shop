from django.core.management.base import BaseCommand
from product.models import Category
from product.helpers.generate_random import generate_random_name
class Command(BaseCommand):
    #this command creates n number of random categories for product
    help = 'Takes one int type argument. Create n number of random product categories.' \
           ' If n is not specified the number of random product categories created is 2'

    def add_arguments(self, parser):
        parser.add_argument('number', type=int, nargs='?', default=2)
    def handle(self, *args, **options):
        i =  options['number']
        k = 0
        name = []
        while k<i:
            name.append(generate_random_name())
            Category.objects.create(title=name[k])
            self.stdout.write(self.style.SUCCESS('Successfully created %s' % (name[k])))
            k+=1
        self.stdout.write(self.style.SUCCESS('Successfully executed the command'))
