from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from product.views import ProductList
from account.views import WishlistList
from order.views import OrderCreateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('jwtauth/', include('djoser.urls')),
    path('jwtauth/', include('djoser.urls.jwt')),
    path('jwtauth/', include('djoser.urls.authtoken')),
    path('wishlist/', WishlistList.as_view(), name='wishlist'),
    path('make_order/', OrderCreateView.as_view(), name='make_order'),
    path('products/', ProductList.as_view(), name='products'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


